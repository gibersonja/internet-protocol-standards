package main

// https://www.rfc-editor.org/pdfrfc/rfc792.txt.pdf

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
	"strconv"
	"time"
)

var verbose bool = false
var data []byte
var err error

func main() {

	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			data = append(data, scanner.Bytes()...)
		}

		err = scanner.Err()
		er(err)
	}

	var msgType byte = 8                  // Type All
	var msgCode byte                      // Type All
	var msgChecksum [2]byte               // Type All
	var msgPointer byte                   // Type 12
	var msgGatewayInternetAddress [4]byte // Type 5
	var msgIdentifier [2]byte             // Type 8,0,13,14,15,16
	var msgSequenceNumber [2]byte         // Type 8,0,13,14,15,16

	var msg []byte
	var destIP string

	/*
		#############
		### TYPES ###
		#############

		0  Echo Reply
		3  Destination Unreachable
		4  Source Quench
		5  Redirect
		8  Echo
		11 Time Exceeded
		12 Parameter Problem
		13 Timestamp
		14 Timestamp Reply
		15 Information Request
		16 Information Reply
	*/

	if len(os.Args) > 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "--help" || arg == "-h" {
				fmt.Print("-h | --help    Print this help message.\n")
				fmt.Printf("-t | --type    ICMP Type, Default:%d\n", msgType)
				fmt.Printf("-i | --ident   Identifier, Default:%d\n", (uint16(msgIdentifier[0])<<8)+uint16(msgIdentifier[1]))
				fmt.Printf("-s | --seq     Initial sequence number, Default:%d\n", (uint16(msgSequenceNumber[0])<<8)+uint16(msgIdentifier[1]))
				fmt.Printf("-c | --code    Message Code, Default:%d\n", msgCode)
				fmt.Printf("-p | --payload Data to send, also accepts from STDIN, Default:%s\n", string(data))
				fmt.Print("               For types 3, 4, 5, 11 and 12: The payload is truncated and assumed to be:\n")
				fmt.Print("               \"Internet Header + 64 bits of Data Datagram\"\n")
				fmt.Print("-P | --point   Pointer value, used with type 12\n")
				fmt.Print("-d | --dest    Destination IP address\n")
				fmt.Print("-g | --gateway Gateway internet address\n")
				fmt.Printf("-v | --verbose Show raw packet, Default:%t\n", verbose)

				fmt.Print("\n")
				return
			}

			if (arg == "-t" || arg == "--type") && i+1 < len(args) {
				tmpVal, err := strconv.Atoi(args[i+1])
				er(err)
				msgType = byte(tmpVal)
			}

			if (arg == "-i" || arg == "--ident") && i+1 < len(args) {
				tmpVal, err := strconv.Atoi(args[i+1])
				er(err)
				msgIdentifier[0] = byte(tmpVal >> 8)
				msgIdentifier[1] = byte(tmpVal)
			}

			if (arg == "-s" || arg == "--seq") && i+1 < len(args) {
				tmpVal, err := strconv.Atoi(args[i+1])
				er(err)
				msgSequenceNumber[0] = byte(tmpVal >> 8)
				msgSequenceNumber[1] = byte(tmpVal)
			}

			if (arg == "-p" || arg == "--payload") && i+1 < len(args) {
				data = []byte(args[i+1])
			}

			if (arg == "-d" || arg == "--dest") && i+1 < len(args) {
				destIP = args[i+1]
			}

			if (arg == "-c" || arg == "--code") && i+1 < len(args) {
				tmpVal, err := strconv.Atoi(args[i+1])
				er(err)
				msgCode = byte(tmpVal)
			}

			if arg == "-v" || arg == "--verbose" {
				verbose = true
			}

			if (arg == "-P" || arg == "--point") && i+1 < len(args) {
				tmpVal, err := strconv.Atoi(args[i+1])
				er(err)
				msgPointer = byte(tmpVal)
			}

			if (arg == "-g" || arg == "--gateway") && i+1 < len(args) {
				regex := regexp.MustCompile(`^(\d+)\.(\d+)\.(\d+)\.(\d+)$`)
				if regex.MatchString(args[i+1]) {
					regexMatch := regex.FindStringSubmatch(args[i+1])

					tmp, err := strconv.Atoi(regexMatch[1])
					er(err)
					msgGatewayInternetAddress[0] = byte(tmp)

					tmp, err = strconv.Atoi(regexMatch[2])
					er(err)
					msgGatewayInternetAddress[1] = byte(tmp)

					tmp, err = strconv.Atoi(regexMatch[3])
					er(err)
					msgGatewayInternetAddress[2] = byte(tmp)

					tmp, err = strconv.Atoi(regexMatch[4])
					er(err)
					msgGatewayInternetAddress[3] = byte(tmp)
				}
			}
		}
	}

	if len(data) == 0 {
		data = []byte("https://www.youtube.com/watch?v=dQw4w9WgXcQ")
	}

	// Assemble Message w/ 0 checksum

	msg = append(msg, msgType, msgCode, msgChecksum[0], msgChecksum[1])

	switch msgType {
	case 0:
		msg = append(msg, msgIdentifier[0], msgIdentifier[1], msgSequenceNumber[0], msgSequenceNumber[1])
		msg = append(msg, data...)
	case 3:
		msg = append(msg, 0, 0, 0, 0)
		msg = append(msg, data...)
	case 4:
		msg = append(msg, 0, 0, 0, 0)
		msg = append(msg, data...)
	case 5:
		msg = append(msg, 0, 0, 0, 0)
		msg = append(msg, msgGatewayInternetAddress[0], msgGatewayInternetAddress[1], msgGatewayInternetAddress[2], msgGatewayInternetAddress[3])
		msg = append(msg, data...)
	case 8:
		msg = append(msg, msgIdentifier[0], msgIdentifier[1], msgSequenceNumber[0], msgSequenceNumber[1])
		msg = append(msg, data...)
	case 11:
		msg = append(msg, 0, 0, 0, 0)
		msg = append(msg, data...)
	case 12:
		msg = append(msg, msgPointer, 0, 0, 0)
		msg = append(msg, data...)
	case 13:
		// Timestamps are the number of seconds since midnight UTC to the thousandths of a second but converted to an integer
		// So milliseconds then
		msg = append(msg, msgIdentifier[0], msgIdentifier[1], msgSequenceNumber[0], msgSequenceNumber[1])
		t := uint32(time.Since(time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)).Milliseconds())
		if t > 86400000 {
			t = uint32(time.Since(time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)).Milliseconds()) - 86400000

		}
		msg = append(msg, byte(t>>24), byte(t>>16), byte(t>>8), byte(t)) // Originate Timestamp
		msg = append(msg, byte(t>>24), byte(t>>16), byte(t>>8), byte(t)) // Receive Timestamp
		msg = append(msg, byte(t>>24), byte(t>>16), byte(t>>8), byte(t)) // Transmit Timestamp
	case 14:
		msg = append(msg, msgIdentifier[0], msgIdentifier[1], msgSequenceNumber[0], msgSequenceNumber[1])
		t := uint32(time.Since(time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)).Milliseconds())
		if t > 86400000 {
			t = uint32(time.Since(time.Date(time.Now().Year(), time.Now().Month(), time.Now().Day(), 0, 0, 0, 0, time.UTC)).Milliseconds()) - 86400000

		}
		msg = append(msg, byte(t>>24), byte(t>>16), byte(t>>8), byte(t)) // Originate Timestamp
		msg = append(msg, byte(t>>24), byte(t>>16), byte(t>>8), byte(t)) // Receive Timestamp
		msg = append(msg, byte(t>>24), byte(t>>16), byte(t>>8), byte(t)) // Transmit Timestamp	case 15:
		msg = append(msg, msgIdentifier[0], msgIdentifier[1], msgSequenceNumber[0], msgSequenceNumber[1])
	case 15:
		msg = append(msg, msgIdentifier[0], msgIdentifier[1], msgSequenceNumber[0], msgSequenceNumber[1])
	case 16:
		msg = append(msg, msgIdentifier[0], msgIdentifier[1], msgSequenceNumber[0], msgSequenceNumber[1])
	default:
		er(fmt.Errorf("Invalid type of (%d) specified", msgType))
	}

	if len(msg)%2 == 1 {
		msg = append(msg, 0)
	}

	msgChecksum = cksum(msg)
	msg[2] = msgChecksum[0]
	msg[3] = msgChecksum[1]

	sendMsg(msg, destIP)
}

func sendMsg(msg []byte, destIP string) {
	//	time.Sleep(time.Duration(1000) * time.Millisecond)
	conn, err := net.Dial("ip4:1", destIP)
	er(err)
	defer conn.Close()

	_, err = conn.Write(msg)
	er(err)

	if verbose {
		for i := 0; i < len(msg); i++ {
			if i%4 == 0 {
				fmt.Print("\n")
			}
			fmt.Printf("%02X ", msg[i])
		}
		fmt.Print("\n")
	}
}

func cksum(msg []byte) [2]byte {
	if len(msg)%2 == 1 {
		msg = append(msg, 0)
	}

	var curVal uint32

	for i := 0; i < len(msg); i++ {
		if i%2 == 0 {
			curVal += (uint32(msg[i]) << 8) + uint32(msg[i+1])
			curVal = (curVal & 0xFFFF) + (curVal >> 16)
		}
	}

	return [2]byte{byte(curVal>>8) ^ 0xFF, byte(curVal) ^ 0xFF}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}
