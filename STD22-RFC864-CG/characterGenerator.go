package main

import (
	"fmt"
	"log"
	"net"
	"os"
	"path"
	"runtime"
	"strconv"
)

var port int = 19
var tcp bool = false

// var udp bool = false
var verbose bool = false
var charSet []byte
var err error

func main() {
	if len(os.Args) > 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "-h" || arg == "--help" {
				fmt.Print("-h | --help    Print this help message\n")
				fmt.Printf("-p | --port    Set alternative port number, Default: %d\n", port)
				fmt.Printf("-t | --tcp     Use TCP, Default: %t\n", tcp)
				//fmt.Printf("-u | --udp     Use UDP, Default: %t\n", udp)
				fmt.Printf("-v | --verbose Enable verbose output, Default: %t\n", verbose)

				fmt.Print("\n")
				return
			}

			if arg == "-t" || arg == "--tcp" {
				tcp = true
			}
			/*
				if arg == "-u" || arg == "--udp" {
					udp = true
				}
			*/
			if arg == "-v" || arg == "--verbose" {
				verbose = true
			}

			if (arg == "-p" || arg == "--port") && i+1 < len(args) {
				tmp, err := strconv.Atoi(args[i+1])
				er(err)

				if tmp >= 0 && tmp < 65536 {
					port = tmp
				}
			}
		}
	}

	debug("Generating Character Set:")
	for i := 33; i < 123; i++ {
		charSet = append(charSet, byte(i))
	}
	debug(string(charSet))

	listenerTCP, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	er(err)

	for {
		connTCP, err := listenerTCP.Accept()
		debug(fmt.Sprintf("Accepted Connection From: %s", connTCP.RemoteAddr().String()))
		if err != nil {
			debug(fmt.Sprintf("Error: %v", err))
		}

		sendData(connTCP)
	}
}

func sendData(conn net.Conn) {
	for {
		_, err := conn.Write(charSet)
		if err != nil {
			debug(fmt.Sprintf("Connection Error Received From: %s", conn.RemoteAddr().String()))
			break
		}

		lastChar := charSet[len(charSet)-1]

		for i := len(charSet) - 1; i > 0; i-- {
			charSet[i] = charSet[i-1]
		}

		charSet[0] = lastChar
	}

	return
}

func debug(input string) {
	if verbose {
		fmt.Printf("%v\n", input)
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
