package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path"
	"runtime"
	"strconv"
)

var port int = 69
var verbose bool = false

func main() {
	if len(os.Args) > 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "-h" || arg == "--help" {
				fmt.Print("-h | --help  Print this help message\n")
				fmt.Printf("-p | --port  Specify alternative port number, Default: %d\n", port)

				fmt.Print("\n")
				return
			}

			if (arg == "-p" || arg == "--port") && i+1 < len(args) {
				tmp, err := strconv.Atoi(args[i+1])
				er(err)

				if tmp >= 0 && tmp < 65536 {
					port = tmp
				}
			}

			if arg == "-v" || arg == "--verbose" {
				verbose = true
			}
		}
	}

	/* start server, listen for read or write request
	if read: stat file, open file, read block by block
	send first block as data packet
	wait for ack
	repeate
	break

	if write: wait for data packet
	write block
	send ack
	repeate
	break

	*/

	listenerTCP, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	debug(fmt.Sprintf("Opened Socket: %s", listenerTCP.Addr().String()))
	er(err)

	for {
		bufTCP := make([]byte, 600)

		connTCP, err := listenerTCP.Accept()
		debug(fmt.Sprintf("Accepted Connection From: %s", connTCP.RemoteAddr().String()))
		er(err)

		bufSize, err := connTCP.Read(bufTCP)
		er(err)

		opcode := (uint16(bufTCP[0]) << 8) + uint16(bufTCP[1])

		switch opcode {
		case 1:
			// Read Request
			debug(fmt.Sprintf("Received Read Request (OPCODE=1) For: %s", connTCP.RemoteAddr().String()))
			var filename, mode string

			for i := 2; i < bufSize; i++ {
				if bufTCP[i] == 0 {
					filename = string(bufTCP[2:i])
					debug(fmt.Sprintf("Filename (%s) For: %s", filename, connTCP.RemoteAddr().String()))
					for j := i + 1; j < bufSize; j++ {
						if bufTCP[j] == 0 {
							mode = string(bufTCP[i+1 : j])
							debug(fmt.Sprintf("Mode (%s) For: %s", mode, connTCP.RemoteAddr().String()))
							break
						}
					}
					break
				}
			}

			fileStat, err := os.Stat(filename)
			if err != nil {
				// send error
			} else {
				blocks := fileStat.Size()
				if fileStat.Size()%512 != 0 {
					blocks++
				}

				// open file
				fh, err := os.OpenFile(filename, os.O_RDONLY, 0644)
				if err != nil {
					sendError(connTCP, uint16(1), "File not found") // may be errcode 1 or 2
				} else {

					defer fh.Close()

					bufFile := make([]byte, 512)
					var blockNumber uint16

					for {
						n, err := fh.Read(bufFile)
						if err != nil && err != io.EOF {
							sendError(connTCP, uint16(2), "Access violation")
						} else {

							sendData(connTCP, blockNumber, bufFile)

							for {
								_, err := connTCP.Read(bufTCP)
								if err != nil {
									sendError(connTCP, uint16(0), "Could not read incomming packet")
									continue
								}

								if bufTCP[0] == 0 && bufTCP[1] == 1 && bufTCP[2] == byte(blockNumber>>8) && bufTCP[3] == byte(blockNumber) {
									debug(fmt.Sprintf("ACK Received (%d) For: %s", blockNumber, connTCP.RemoteAddr().String()))
									blockNumber++
									break
								}
							}

							if n < 512 {
								sendData(connTCP, blockNumber, bufFile)
								break
							}
						}
					}
				}
				// read file in chunks with bufio or scanner???
				// read 512 bytes
				// send data packet
				// wait for ack for block number
			}
		case 2:
			// Write Request
		default:
			sendError(connTCP, uint16(4), "Illegal TFTP operation")
		}
	}

}

func sendData(connTCP net.Conn, blockNumber uint16, data []byte) {
	var packet []byte

	packet = append(packet, 0, 3, byte(blockNumber>>8), byte(blockNumber))
	packet = append(packet, data...)

	_, err := connTCP.Write(packet)
	if err != nil {
		sendError(connTCP, 0, fmt.Sprintf("Could not send DATA BLOCK: #%d", blockNumber))
		debug(fmt.Sprintf("Data Block (%d) Sent To: %s", blockNumber, connTCP.RemoteAddr().String()))
	}
}

func sendError(connTCP net.Conn, errCode uint16, errMsg string) {
	var packet []byte

	packet = append(packet, 0, 5, byte(errCode>>8), byte(errCode))
	packet = append(packet, []byte(errMsg)...)
	packet = append(packet, 0)

	_, err := connTCP.Write(packet)
	debug(fmt.Sprintf("Sent Error (%d:%s) To: %s", errCode, errMsg, connTCP.RemoteAddr().String()))
	if err != nil {
		// dont send error packet, might cause loop, close connection instead
		debug(fmt.Sprintf("Error (%v) Closing Connection To: %s", err, connTCP.RemoteAddr().String()))
		connTCP.Close()
	}

}

func debug(input string) {
	if verbose {
		fmt.Printf("%v\n", input)
	}
}

func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}
