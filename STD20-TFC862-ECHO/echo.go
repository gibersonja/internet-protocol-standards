package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"path"
	"regexp"
	"runtime"
)

var data []byte
var tcp bool = false
var udp bool = false
var server bool = false
var verbose bool = false
var socket string
var err error

func main() {
	if isPipe() {
		scanner := bufio.NewScanner(os.Stdin)
		for scanner.Scan() {
			data = append(data, scanner.Bytes()...)
		}

		err = scanner.Err()
		er(err)
	}

	if len(os.Args) > 1 {
		args := os.Args[1:]

		for i := 0; i < len(args); i++ {
			arg := args[i]

			if arg == "-h" || arg == "--help" {
				fmt.Print("-h | --help   Print this help message\n")
				fmt.Print("-t | --tcp     Use TCP\n")
				fmt.Print("-u | --udp     Use UDP\n")
				fmt.Print("-v | --verbose Verbose output\n")
				fmt.Print("A.B.C.D:XY    Socket to on which to listen or send data\n")
				fmt.Print("-s | --server  Start server, otherwise application is client\n")
				fmt.Print("\n")
				fmt.Print("               All data sent as client is read from STDIN\n")
				fmt.Print("               All data received as server is sent to STDOUT\n")

				fmt.Print("\n")
				return
			}

			if arg == "-t" || arg == "--tcp" {
				tcp = true
			}

			if arg == "-u" || arg == "--udp" {
				udp = true
			}

			regex := regexp.MustCompile(`^\d+\.\d+\.\d+\.\d+:\d+`)

			if regex.MatchString(arg) {
				socket = arg
			}

			if arg == "-s" || arg == "--server" {
				server = true
			}

			if arg == "-v" || arg == "--verbose" {
				verbose = true
			}

		}
	}

	if server {
		debug("Server Mode")
		if tcp {
			debug(fmt.Sprintf("TCP Mode, Using Socket: %s", socket))
			l, err := net.Listen("tcp", socket)
			er(err)

			defer l.Close()

			for {
				conn, err := l.Accept()
				debug(fmt.Sprintf("Accepted Connection From: %s", conn.RemoteAddr().String()))
				er(err)

				buf := make([]byte, 0, 4096)
				tmp := make([]byte, 256)

				for {
					n, err := conn.Read(tmp)
					if err != nil {
						if err != io.EOF {
							er(err)
						}
						break
					}

					buf = append(buf, tmp[:n]...)

				}

				debug(fmt.Sprintf("Received Data From: %s", conn.RemoteAddr().String()))
				for j := 0; j < len(buf); j++ {
					fmt.Printf("%c", buf[j])
				}
				debug(fmt.Sprintf("Sending Data To: %s", conn.RemoteAddr().String()))
				conn.Write(buf)
			}

		}

		if udp {
			debug(fmt.Sprintf("UDP Mode, Using Socket: %s", socket))

		}
	} else {
		debug("Client Mode")
		if tcp {
			debug(fmt.Sprintf("TCP Mode, Using Socket: %s", socket))
		}

		if udp {
			debug(fmt.Sprintf("UDP Mode, Using Socket: %s", socket))
		}

	}

}
func er(err error) {
	pc, file, line, ok := runtime.Caller(1)
	if !ok {
		log.Fatal("error getting caller function\n")
		os.Exit(3)
	}

	if err != nil {
		log.Fatalf("| %v | %s:%d | Error: %v\n", runtime.FuncForPC(pc).Name(), path.Base(file), line, err)
	}
}

func isPipe() bool {
	fileInfo, _ := os.Stdin.Stat()
	return fileInfo.Mode()&os.ModeCharDevice == 0
}

func debug(input string) {
	if verbose {
		fmt.Printf("%v\n", input)
	}
}
